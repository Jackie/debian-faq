#
# Makefile for the release notes, top-level
#

# comment out this line when the debian-faq should go into
# production mode
# DRAFT := 1

ifeq "$(DRAFT)" "1"
    draftmode := yes
else
    draftmode := maybe
endif

manual := debian-faq

install_file	:= cp -pr
makedir	:= mkdir -p -m 0755

PUBLISHDIR := $(DESTDIR)/usr/share/doc/debian/FAQ

# WARNING - Editors and translators: the LANGUAGES list is currently sorted
# alphabetically but *NEW* languages that somebody wants to start working in
# should go to the end, that way if the language fails to build, or has errors,
# the other languages are not affected
#
# TODO - Treat 'probationary' languages independently so that they if they
# break they do not break the build for stable translations
LANGUAGES := en de fr it ja nl ru zh-cn

# The following is a list of 'probationary' languages, these might not yet
# build fine and might break the build.  Move them over to LANGUAGES when they
# do:
DISABLED_LANGUAGES :=
ALL_LANGUAGES := $(LANGUAGES) $(DISABLED_LANGUAGES)

# languages without .po files
# (either original English or translators don't like .po)
DBK_LANGUAGES := en
PO_LANGUAGES := $(filter-out $(DBK_LANGUAGES), $(ALL_LANGUAGES))
XMLROFF_LANGS=zh-cn
PO_FILES := $(wildcard */*.po)

# DISABLED_PDF are the languages we will not generate PDF versions for:
DISABLED_PDF=ja zh-cn
# cairo backend of xmlroff 0.6.0 crashes on some languages (#492597)
GPLIST=ja

LANGUAGES-publish := $(addsuffix -publish,$(LANGUAGES))
LANGUAGES-clean := $(addsuffix -clean,$(LANGUAGES))

SOURCES := $(wildcard en/*.dbk)

# DocBook stuff
XP=xsltproc --nonet --novalid --xinclude \
    --stringparam draft.mode $(draftmode)
XL=xmllint --nonet --noout --postvalid --xinclude
# XSL files and parameters
# note: the URL is used as identifier, no HTTP is used!
DOCBOOK_XSL=http://docbook.sourceforge.net/release/xsl/current
# for HTML output
DBK2HTML=$(CURDIR)/html.xsl
# all in one file for text output
DBK2HTML1=$(CURDIR)/txt.xsl
#
# NOTE: to DEBUG the generation of PDFs use the '-d -V' switches
# in DBLATEX and review the temporary files generated for errors
DBLATEX=dblatex --backend=xetex --style=db2latex \
    --xslt-opts=--nonet --no-external \
    --xsl-user=hyphenation.xsl \
    --param=xetex.font='                 \
\setmainfont{CharisSIL-R.ttf}[           \
  BoldFont       = CharisSIL-B.ttf ,     \
  ItalicFont     = CharisSIL-I.ttf ,     \
  BoldItalicFont = CharisSIL-BI.ttf]     \
\setsansfont{FreeSans}                   \
\setmonofont{FreeMono}' \
    --param=draft.mode=$(draftmode) \
    --param=format=a4 \
    --param=glossterm.auto.link=1 \
    --param=lingua=$(LINGUA) \
    --param=monoseq.hyphenation=nohyphen
PROFILE=$(DOCBOOK_XSL)/profiling/profile.xsl
PREPROC=$(CURDIR)/preproc.xsl
DBLATEXXSL=$(CURDIR)/dblatex.xsl
XMLROFF=xmlroff --backend cairo
XMLROFF_GP=xmlroff --backend gp

FORMATS=html txt pdf

# po4a stuff
# Mimic options that used to be hardcoded in local Locale::Po4a::Xml
PO4AOPTIONS=-o nodefault='<contrib> <editor> <indexterm> <note> <primary> <pubdate> <programlisting> <orderedlist> <screen>' -o translated='<contrib> <editor> W<programlisting> <note> <orderedlist> <pubdate> W<screen> <indexterm><primary>' -o inline='<indexterm> <primary>'
UPDATEPO=po4a-updatepo $(PO4AOPTIONS) --previous --format docbook
# The "--keep 0" should be removed as soon as the translations are ready
TRANSLATE=po4a-translate $(PO4AOPTIONS) --format docbook --keep 0

.SUFFIXES:
.PHONY: publish all clean $(LANGUAGES-publish)

all:
	for l in $(LANGUAGES); do \
    echo "Creating documents for language $$l"; \
    $(MAKE) LINGUA=$$l $(FORMATS); \
	done

# TODO: Do not use 'set -e' here. if one of the files here is missing for one
# language due to a failed build the installation will break the installation
# for the other languages.
# We could change this into a proper Makefile goal with its subtargets
# but it needs to be rewritten to do so
install:
	@echo "Publishing to '$(PUBLISHDIR)'"
	$(MAKE) $(FORMATS); \
	$(makedir) $(PUBLISHDIR); \
	for lang in $(LANGUAGES); do \
		$(makedir) $(PUBLISHDIR)/$$lang; \
		[ ! -d "$$lang/$$lang/" ] || \
		$(install_file) $$lang/$$lang/* \
		$(PUBLISHDIR)/$$lang/; \
		[ ! -e "$$lang/$(manual).$$lang.txt" ] ||  \
		$(install_file) $$lang/$(manual).$$lang.txt $(PUBLISHDIR); \
		[ ! -e "$$lang/$(manual).$$lang.pdf" ] ||  \
		$(install_file) $$lang/$(manual).$$lang.pdf $(PUBLISHDIR); \
	done

ifdef LINGUA
html::	$(LINGUA)/$(LINGUA)/index.$(LINGUA).html
$(LINGUA)/$(LINGUA)/index.$(LINGUA).html: \
		$(LINGUA)/$(manual).$(LINGUA).xml html.xsl
	@test -d /usr/share/xml/docbook/stylesheet/nwalsh/images/  || { echo "ERROR: The Docbook stylesheet's images are not available. Please install the docbook-xsl package." ; false ; }
	mkdir -p $(LINGUA)/$(LINGUA)/images
	cp debian.css $(LINGUA)/$(LINGUA)
	cp /usr/share/xml/docbook/stylesheet/nwalsh/images/*.png \
	    $(LINGUA)/$(LINGUA)/images
	cd $(LINGUA) && $(XP) -o $(LINGUA)/ \
	    --stringparam html.ext .$(LINGUA).html \
	    $(DBK2HTML) $(manual).$(LINGUA).xml

pdf::	$(LINGUA)/$(manual).$(LINGUA).pdf
ifneq "$(filter $(XMLROFF_LANGS),$(LINGUA))" ""
$(LINGUA)/$(manual).$(LINGUA).pdf: \
		$(LINGUA)/$(manual).$(LINGUA).fo
$(LINGUA)/$(manual).$(LINGUA).fo: \
		$(LINGUA)/$(manual).$(LINGUA).xml fo.xsl
else
$(LINGUA)/$(manual).$(LINGUA).pdf: \
		$(LINGUA)/$(manual).$(LINGUA).xml
endif

ifneq "$(filter $(DISABLED_PDF),$(LINGUA))" ""
	cp nopdf.pdf $(LINGUA)/$(manual).$(LINGUA).pdf
else
ifneq "$(filter $(XMLROFF_LANGS),$(LINGUA))" ""
%.fo: %.xml xmlroff.xsl
	$(XP) xmlroff.xsl $< |$(XP) --output $@ fo.xsl -

%.pdf: %.fo
	@test -n "`which xmlroff`"  || { echo "ERROR: xmlroff not found. Please install the xmlroff package." ; false ;  }
	USE_GP=`echo " $(GPLIST) " | grep " $$LINGUA "`; \
	if [ -n "$$USE_GP" ]; then \
	    $(XMLROFF_GP) -o $@ $<; \
	else \
	    $(XMLROFF) -o $@ $<; \
	fi
else
%.pdf: %.xml
	@test -n "`which dblatex`"  || { echo "ERROR: dblatex not found. Please install the dblatex package." ; false ;  }
	export TEXINPUTS="$$(pwd)/$(@D):"; \
	$(XP) $(DBLATEXXSL) $< | $(DBLATEX) --output=$@ -
endif
endif

txt::	$(LINGUA)/$(manual).$(LINGUA).txt
$(LINGUA)/$(manual).$(LINGUA).txt: \
		$(LINGUA)/$(manual).$(LINGUA).xml txt.xsl
	@test -n "`which xsltproc`"  || { echo "ERROR: xsltproc not found. Please install the xsltproc package." ; false ;  }
	@test -n "`which w3m`"  || { echo "ERROR: w3m not found. Please install the w3m package." ; false ;  }
ifneq "$(findstring $(LINGUA) , ja )" ""
	$(XP) $(DBK2HTML1) $< > ja/all-in-one.html
	perl ja/kinsoku.pl
	cat ja/temp.html | w3m -o display_charset=UTF-8 -cols 70 -dump -no-graph -T text/html > $@
	rm -f ja/all-in-one.html ja/temp.html
else
	$(XP) $(DBK2HTML1) $< \
	    | w3m -o display_charset=UTF-8 -cols 70 -dump -no-graph -T text/html > $@
endif

$(LINGUA)/$(manual).$(LINGUA).xml: \
		$(patsubst en/%,$(LINGUA)/%,$(SOURCES)) $(manual).ent
	$(XP) $(PROFILE) $(@D)/$(manual).dbk | $(XP) $(PREPROC) - > $@
endif

FORCE:

# There must be an easier way than recursive make!
.PRECIOUS:		%.dbk
ifndef LINGUA
%.dbk: FORCE
	$(MAKE) $@ LINGUA=`basename $(@D)`

else
ifeq "$(findstring  $(LINGUA) , $(DBK_LANGUAGES) )" ""
$(LINGUA)/%.dbk:	en/%.dbk $(LINGUA)/%.po
	@test -n "`which po4a`"  || { echo "ERROR: po4a not found. Please install the po4a package." ; false ;  }
	$(TRANSLATE) --master $< --po $(@:.dbk=.po) --localized $@
endif
endif

ifdef LINGUA
.PHONY:	updatepo
update-po: updatepo
updatepo:	$(patsubst en/%.dbk,$(LINGUA)/%.po-force,$(wildcard en/*.dbk))
# The next is just a PHONY target and forcible updates the PO files
$(LINGUA)/%.po-force: FORCE
	$(UPDATEPO) --master en/$*.dbk --po $(LINGUA)/$*.po

validate:	$(patsubst en/%,$(LINGUA)/%,$(SOURCES))
	$(XL) $(LINGUA)/$(manual).dbk
endif

tidypo:
ifdef LINGUA
	for po in $(wildcard $(LINGUA)/*.po); do \
	    msgcat $$po > $$po.tmp && mv $$po.tmp $$po; \
	done
else
	for po in $(wildcard */*.po); do \
	    msgcat $$po > $$po.tmp && mv $$po.tmp $$po; \
	done
endif

ifndef LINGUA

# Iterate over all PO based languages only
updatepo:
	for l in $(PO_LANGUAGES); do \
	    $(MAKE) $@ LINGUA=$$l; \
	done

%:
	for l in $(LANGUAGES); do \
	    $(MAKE) $@ LINGUA=$$l; \
	done
endif

clean:: $(LANGUAGES-clean)

$(LANGUAGES-clean):
	LINGUA=$(subst -clean,,$@); \
	cd $$LINGUA; \
	echo " $(DBK_LANGUAGES) " | grep -q " $$LINGUA " || rm -f *.dbk; \
	rm -rf *~ .*~ $$LINGUA $(manual).*.txt $(manual).*.pdf \
	    $(manual).$$LINGUA.xml ifxetex.sty body.tmp head.tmp
