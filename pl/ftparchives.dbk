<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="ftparchives"><title>Archiwa FTP Debiana</title>
<section id="dirtree"><title>Do czego służą te wszystkie katalogi w archiwach FTP Debiana?</title>
<para>
Programy przystosowane dla Debiana dostępne są w jednym z kilku katalogów na
każdym serwerze lustrzanym Debiana.
</para>
<para>
Katalog <literal>dists</literal> jest skrótem od "distributions" (z ang.
dystrybucje) i jest miejscem, gdzie umieszczane są aktualnie dostępne wydania
(i zapowiedzi wydań).
</para>
<para>
Katalog <literal>pool</literal> zawiera aktualne pakiety, zobacz <xref
linkend="pools"/>.
</para>
<para>
Dodatkowo udostępnione są następujące katalogi:
</para>
<variablelist>
<varlistentry>
<term><emphasis>/tools/</emphasis>:</term>
<listitem>
<para>
Narzędzia DOS'owe do tworzenia dyskietek startowych, dzielenia dysku na
partycje, kompresji i dekompresji plików oraz startowania Linuksa.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis>/doc/</emphasis>:</term>
<listitem>
<para>
Podstawowa dokumentacja Debiana, czyli FAQ (najczęściej zadawane pytania),
informacje o systemie zgłaszania błędów itp.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis>/indices/</emphasis>:</term>
<listitem>
<para>
Pliki Maintainers i override.[The Maintainers file and the override files.]
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis>/project/</emphasis>:</term>
<listitem>
<para>
głównie materiały opiekunów takie jak:
</para>
<variablelist>
<varlistentry>
<term><emphasis>project/experimental/</emphasis>:</term>
<listitem>
<para>
Ten katalog zawiera pakiety i narzędzia, nad którymi ciągle trwają prace i
są aktualnie na etapie testów alfa.  Użytkownicy nie powinni tych pakietów
używać, ponieważ może to być niebezpieczne, nawet dla najbardziej
doświadczonych.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis>project/orphaned/</emphasis>:</term>
<listitem>
<para>
Pakiety osierocone, czyli porzucone przez swoich opiekunów i wycofane z
dystrybucji.
</para>
</listitem>
</varlistentry>
</variablelist>
</listitem>
</varlistentry>
</variablelist>
</section>

<section id="dists"><title>Ile dystrybucji Debiana jest w katalogu <literal>dists</literal>?</title>
<para>
Zazwyczaj są tam trzy dystrybucje, "stable" (stabilna), "testing" (testowana),
"unstable" (niestablina, rozwijana).  Czasami jest także dystrybucja "frozen"
(zamrożona) (zob.  <xref linkend="frozen"/>).
</para>
</section>

<section id="codenames"><title>A co z tymi wszystkimi nazwami jak "slink", "potato" itp.?</title>
<para>
Są to tylko swego rodzaju "kryptonimy".  Kiedy dystrybucja Debiana jest w
fazie przygotowywania, nie ma numeru wersji, tylko kryptonim.  Zostały one
nadane, żeby ułatwić pracę serwerom lustrzanym i uniknąć zbędnego
kopiowania, kiedy dystrybucja np.  zmienia stan z niestabilnego na stabilny.
</para>
<para>
Aktualnie, <literal>stable</literal> jest dowiązaniem symbolicznym do
<literal>woody</literal> (i.e.  Debian GNU/Linux 9) a
<literal>testing</literal> do <literal>sarge</literal>.  Oznacza to, że
<literal>woody</literal> jest aktualnie dystrybucją stabilną, a
<literal>sarge</literal> jest w fazie testów.
</para>
<para>
<literal>unstable</literal> jest stałym dowiązaniem symbolicznym do
<literal>sid</literal>, jako że <literal>sid</literal> jest zawsze wersją
niestabilną (see <xref linkend="sid"/>).
</para>
<section id="oldcodenames"><title>Jakie inne nazwy były wcześniej?</title>
<para>
Wcześniej używano nazw: <literal>buzz</literal> dla wydania 1.1,
<literal>rex</literal> dla wydania 1.2, <literal>bo</literal> dla wydania
1.3.x, <literal>hamm</literal> dla wydania 2.0, <literal>slink</literal> dla
wydania 2.1 i <literal>potato</literal> dla wydania 2.2.
</para>
</section>

<section id="sourceforcodenames"><title>W jaki sposób powstają nazwy wydań?</title>
<para>
Jak na razie były zapożyczane z filmu "Toy Story" wytwórni Pixar.
</para>
<itemizedlist>
<listitem>
<para>
<emphasis>buzz</emphasis> - (Buzz Lightyear) astronauta,
</para>
</listitem>
<listitem>
<para>
<emphasis>rex</emphasis> - tyranozaur,
</para>
</listitem>
<listitem>
<para>
<emphasis>bo</emphasis> - (Bo Peep) dziewczynka, która zaopiekowała się
owieczką,
</para>
</listitem>
<listitem>
<para>
<emphasis>hamm</emphasis> - świnka-skarbonka,
</para>
</listitem>
<listitem>
<para>
<emphasis>slink</emphasis> - (Slinky Dog) "przegubowy" pies,
</para>
</listitem>
<listitem>
<para>
<emphasis>potato</emphasis> - oczywiście Pan Ziemiak,
</para>
</listitem>
<listitem>
<para>
<emphasis>woody</emphasis> - drewniany kowboj,
</para>
</listitem>
<listitem>
<para>
<emphasis>sarge</emphasis> - sierżant w armii plastikowych żołnierzyków.
</para>
</listitem>
</itemizedlist>
</section>

</section>

<section id="frozen"><title>A co z "frozen"?</title>
<para>
Kiedy dystrybucja "testing" jest wystarczająco dojrzała, kierownik wydania
zaczyna ją "zamrażać".  Zmniejszane jest tempo aktualizacji pakietów, aby
ograniczyć ilość błedów, które pojawiają się w wersji niestabilnej,
przenoszonych do testowej.
</para>
<para>
Po pewnym czasie, dystrybucja "testing" jest całkowicie zamrożona, co oznacza
wstrzymanie aktualizacji pakietów, o ile nie zawierają krytycznych dla
wydania błędów.  Dystrybucja zamrożona przechodzi przez kilkumiesięczny
okres próbny składający się na przemian z aktualizacji i z okresów
głębokiego zamrożenia nazywanych "cyklami testowymi".
</para>
<para>
Prowadzimy listę błędów w dystrybucji "testing", które mogą spowodować
wstrzymanie konkretnego pakietu, lub całego wydania.  W momencie, kiedy liczba
takich błędów spadnie poniżej maksymalnej akceptowalnej wartości,
dystrybucja uznawana jest za stabilną i wydawana z kolejnym numerem wersji.
</para>
<para>
Wraz z nowym wydaniem, zdezaktualizowana poprzednia dystrybucja "stable"
przenoszona jest do archiwum.  Więcej informacji znajdziesz pod adresem <ulink
url="http://www.debian.org/distrib/archive">Debian - archiwum</ulink>.
</para>
</section>

<section id="sid"><title>Dystrybucja "sid"?</title>
<para>
<emphasis>sid</emphasis> czyli <emphasis>unstable</emphasis> jest miejscem, do
którego trafia większość nowych pakietów.  Dystrybucja ta nigdy nie
doczeka się bezpośredniego wydania, ponieważ pakiety przeznaczone do wydania
muszą najpierw wejść do dystrybucji <emphasis>testing</emphasis>, aby mogły
być poźniej wydane w dystrybucji <emphasis>stable</emphasis>.
<emphasis>sid</emphasis> zawiera pakiety zarówno dla architektur
przeznaczonych do wydania jak i tych nie wydawanych.
</para>
<para>
Nazwa "sid" także pochodzi z filmu "Toy Story": Sid był chłopcem z
sąsiedztwa, który psuł zabawki :-)
</para>
<section id="sid-history"><title>Zapiski historyczne o dystrybucji "sid"</title>
<para>
Kiedy dzisiejszy Sid jeszcze nie istniał, organizacja sieciowych archiwów
Debiana miała jedną dużą wadę: kiedy dokładano nową architekturę do
bieżącej dystrybucji unstable, pakiety zrobione dla niej mogły być wydane
dopiero wtedy, gdy ta dystrybucja stawała się nową dystrybucją stable.  Dla
wielu architektur nie dochodziło do tego i trzeba było przenosić
odpowiadające im katalogi, gdy dochodziło do wydania dystrybucji.  Było to
niepraktyczne, ponieważ przenoszenie katalogów silnie obciążało łącza.
</para>
<para>
Administratorzy archiwów sieciowych przez kilka lat obchodzili ten problem,
umieszczając binaria dla architektur jeszcze nie wydanych w specjalnym
katalogu o nazwie sid.  Dla architektur jeszcze nie wydanych, tworzono w chwili
wydania dowiązanie z aktualnego katalogu stable do sid i od tej pory tworzono
je w drzewie unstable, jak zwykle.  Takie rozwiązanie było trochę mylące
dla użytkowników.
</para>
<para>
Z nadejściem katalogu ,,pool'' (zobacz <xref linkend="pools"/>) zaczęto
zapisywać pakiety binarne w lokalizacji kanonicznej w tymże katalogu,
niezależnie od dystrybucji, więc wydanie dystrybucji przestało być
związane z poddawaniem serwerów lustrzanych dużym obciążeniom (natomiast
mamy do czynienia z dość sporymi, rozłożonymi w czasie obciążeniami w
trakcie całego procesu rozwijania dystrybucji).
</para>
</section>

</section>

<section id="stable"><title>Co zawiera katalog stable?</title>
<itemizedlist>
<listitem>
<para>
stable/main/: Ten katalog zawiera pakiety oficjalnie uznawane za najbardziej
aktualne wydanie systemu Debian GNU/Linux.
</para>
<para>
Wszystkie z tych pakietów są zgodne z <ulink
url="http://www.debian.org/social_contract#guidelines">Wytycznymi Debiana
dotyczącymi Oprogramowania Wolnodostępnego</ulink> i można je swobodnie
używać, a także rozpowszechniać.
</para>
</listitem>
<listitem>
<para>
stable/non-free/: Ten katalog zawiera informacje o pakietach, których
rozpowszechnianie zostało ograniczone przez wymagania stawiane dystrybutorowi,
które mówią o zwróceniu szczególnej uwagi, na kwestie praw autorskich
danego programu.
</para>
<para>
Na przykład, licencje niektórych pakietów zabraniają komercyjnego
rozpowszechniania.  Inne znowu mogą być redystrybuowane, ale są typu
shareware, a nie wolnym oprogramowaniem.  Zanim włączy się którykolwiek z
tych pakietów do jakiejś redystrybucji (np.  na CD-ROMie), należy
przestudiować jego licencję i prawdopodobnie przeprowadzić odpowiednie
negocjacje.
</para>
</listitem>
<listitem>
<para>
stable/contrib/: Ten katalog zawiera informacje o pakietach wolnych w
rozumieniu DFSG (Debian Free Software Guidelines) i podlegających swobodnemu
rozpowszechnianiu, ale w jakiś sposób zależnych od pakietu, który
swobodnemu rozpowszechnianiu nie podlega i z tej przyczyny jest dostępny w
sekcji non-free.
</para>
</listitem>
</itemizedlist>
</section>

<section id="testing"><title>Co zawiera katalog testing?</title>
<para>
Pakiety są przenoszone do katalogu testing po tym gdy przejdą stosowny okres
testowania w unstable.  Muszą być zsynchronizowane ze wszystkimi
architekturami na których były zbudowane i nie mogę posiadać zależności,
które czyniłyby je niemożliwymi do zainstalowania.  Muszą również
posiadać mniej błędów krytycznych dla wydania od wersji która jest
dostępna w testing.  W ten sposób mamy nadzieję, że 'testing' jest zawsze
bliżej by stać się kandydatem do wydania.
</para>
<para>
Więcej informacji o ogólnym stanie "testing" oraz o poszczególnych pakietach
dostępnych jest na <ulink
url="http://www.debian.org/devel/testing">http://www.debian.org/devel/testing</ulink>
</para>
</section>

<section id="unstable"><title>Co zawiera katalog unstable?</title>
<para>
Katalog 'unstable' to aktualny stan prac deweloperów.  Zapraszamy
użytkowników do używania i testowania tych pakietów, ale ostrzegamy, o ich
stanie gotowości.  Przewagą używania dystrybucji niestabilnej jest fakt
bycia zawsze na czasie z nowinkami systemu GNU/Linux, ale jeśli coś pójdzie
nie tak to nie licz na pomoc ;)
</para>
<para>
W 'unstable' istnieją również podkatalogi main, contrib, non-free o takim
samym przeznaczeniu jak w 'stable',
</para>
</section>

<section id="archsections"><title>Czym są te wszystkie katalogi wewnątrz <literal>dists/stable/main</literal>?</title>
<para>
W każdym z głównych drzew katalogów (<literal>dists/stable/main</literal>,
<literal>dists/stable/contrib</literal>,
<literal>dists/stable/non-free</literal>,
<literal>dists/unstable/main/</literal>, itd.), pakiety binarne przechowywane
są w podkatalogach których nazwy wskazują na jaką architekturę zostały
skompilowane:
</para>
<itemizedlist>
<listitem>
<para>
binary-all/, dla pakietów, które są niezależne od architektury.  Katalog
zawiera na przykład skrypty Perla lub czystą dokumentację.
</para>
</listitem>
<listitem>
<para>
binary-i386/, dla pakietów uruchamianych na maszynach PC 80x86.
</para>
</listitem>
<listitem>
<para>
binary-m68k/, dla pakietów uruchamianych na maszynach opartych na procesorach
Motorola 680x0.  Aktualnie jest to robione głównie dla komputerów Atari i
Amiga oraz dla niektórych płyt przemysłowych opartych na standardzie VME.
</para>
</listitem>
<listitem>
<para>
binary-sparc/, dla pakietów uruchamianych na stacjach Sun SPARC.
</para>
</listitem>
<listitem>
<para>
binary-alpha/, dla pakietów uruchamianych na maszynach DEC Alpha.
</para>
</listitem>
<listitem>
<para>
binary-powerpc/, dla pakietów uruchamianych na maszynach PowerPC.
</para>
</listitem>
<listitem>
<para>
binary-arm/, dla pakietów uruchamianych na maszynach ARM.
</para>
</listitem>
</itemizedlist>
<para>
Zauważ, że aktualne pakiety binarne dla <emphasis>woodiego</emphasis> i
kolejnych wydań nie znajdują się już w tych katalogach, ale w głównym
katalogu <literal>pool</literal>.  Pliki indeksów (Packages i Packages.gz)
zostały zostawione dla zgodności.
</para>
<para>
Zobacz <xref linkend="arches"/> by dowiedzieć się więcej.
</para>
</section>

<section id="source"><title>Gdzie jest kod źródłowy?</title>
<para>
Udostępniony jest kod źródłowy każdego elementu systemu Debian.  Co
więcej, ustalenia licencji większości programów w systemie
<emphasis>nakazują</emphasis> aby kod źródłowy, albo informacja, gdzie jest
udostępniony była rozpowszechniana razem z programami.
</para>
<para>
Zazwyczaj kod źródłowy jest umieszczony w katalogach "source", które są
umieszczone w tym samym miejscu, co katalogi z binariami przeznaczonymi dla
konkretnych architektur.  Aktualnie można go znaleźć w katalogu
<literal>pool</literal> (zobacz <xref linkend="pools"/>).  Aby pobrać
źródła bez zagłębiania się w strukturę archiwum, spróbuj użyć
polecenia <literal>apt-get source nazwapakietu</literal>.
</para>
<para>
Niektóre pakiety, ze względu na ograniczenia zawarte w ich licencji,
publikowane są tylko w postaci źródeł.  Przykładem takiego pakietu jest
<literal>pine</literal>, zajrzyj do <xref linkend="pine"/> aby dowiedzieć sie
więcej.
</para>
<para>
Kod źródłowy może, ale nie musi być dostępny dla pakietów w katalogach
"contrib" i "non-free", które formalnie nie są częścią systemu Debian.
</para>
</section>

<section id="pools"><title>Co jest w katalogu <literal>pool</literal>?</title>
<para>
Dawniej pakiety były składowane w podkatalogach katalogu
<literal>dists</literal> odpowiadających dystrybucji, która je zawierała.
Wynikały z tego różne trudności, np.  serwery lustrzane musiały pobierać
duże ilości danych, kiedy wprowadzano większe poprawki.
</para>
<para>
Pakiety przechowywane są w wielkim "worku" ("pool"), którego struktura
oparata jest na nazwach pakietów źródłowych.  Aby dało się nim łatwiej
zarządzać, podzielono go na podkatalogi według sekcji ("main", "contrib",
"non-free") oraz pierwszych liter nazw pakietów.  W tym drzewie umieszczone
są katalogi dla każdego pakietu, które zawierają binaria dla wszystkich
architektur oraz źródła, z których je wygenerowano.
</para>
<para>
Możesz sprawdzić gdzie jest umieszczony pakiet wydając polecenie
<literal>apt-cache showsrc nazwapakietu</literal> i odczytując linię
"Directory:".  Na przykład pakiety <literal>apache</literal> są przechowywane
w <literal>pool/main/a/apache/</literal>.  Ponieważ jest bardzo dużo
pakietów <literal>lib*</literal>, są one traktowane w nieco odmienny sposób:
na przykład pakiety libpaper umieszczone są w
<literal>pool/main/libp/libpaper/</literal>.
</para>
<para>
Katalogi <literal>dists</literal> są ciągle używane do przechowywania
plików z indeksami przez programy takie jak <literal>apt</literal>.  Także,
starsze dystrybucje nie zostały przystosowane do używania mechanizmu "pools".
</para>
<para>
Nie musisz zaprzątać sobie głowy tymi informacjami, <literal>apt</literal> i
przypuszczalnie <literal>dpkg-ftp</literal> (zobacz <xref
linkend="howtocurrent"/>) zajmą się tym za ciebie.
</para>
</section>

<section id="incoming"><title>Co jest w "incoming"?</title>
<para>
Kiedy opiekun przesyła pakiet do archiwum, umieszcza się go w katalogu
"incoming" do czasu potwierdzenia jego pochodzenia i sprawdzenia, czy
rzeczywiście ma zostać tam umieszczony.
</para>
<para>
Zazwyczaj nikt nie powinien instalować pakietów z tego katalogu.  Jednakże
dla bardzo rzadkich przypadków awaryjnych jest on udostępniony pod adresem
<ulink url="http://incoming.debian.org/">http://incoming.debian.org/</ulink>.
Możesz pobrać z niego pakiety samodzielnie, sprawdzić podpisy GPG i sumy
kontrolne MD5 w plikach .changes i .dsc a następnie je zainstalować.
</para>
</section>

</chapter>

