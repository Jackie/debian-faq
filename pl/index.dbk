<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
    "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
<!-- Include entity definition file by uncommenting the following -->
<!-- <!ENTITY % versiondata SYSTEM "version.ent"> %versiondata;   -->
]>

<book lang="pl">

<title>Debian GNU/Linux FAQ</title>

<bookinfo>

<authorgroup>
<author><personname><xref linkend="authors"/></personname></author>
<author><personname><xref linkend="translators"/></personname></author>

</authorgroup>
<releaseinfo>wersja 9.0, 9 November 2018</releaseinfo>

<pubdate><!-- put date --></pubdate>


<abstract>
<para>
Ten dokument odpowiada na często zadawane pytania dotyczące systemu Debian
GNU/Linux.
</para>
</abstract>

<copyright><year>1996-2003</year><holder>by Software in the Public Interest</holder></copyright>
<legalnotice>
<para>
Udziela się zgody na dystrybuowanie wiernych kopii tego dokumentu pod
warunkiem opatrzenia wszystkich kopii informacją o prawach autorskich a także
niniejszym pozwoleniem.
</para>
<para>
Udziela się zgody na tworzenie i dystrybucję zmodyfikowanych wersji tego
dokumentu na warunkach takich, jak w przypadku wiernych kopii, pod warunkiem,
że całe zmodyfikowane dzieło rozprowadzane jest na zasadach identycznych do
zawartych w niniejszym pozwoleniu.
</para>
<para>
Udziela się zgody na kopiowanie i dystrybucję tłumaczeń tego dokumentu w
innym języku, zgodnie z powyższymi warunkami dotyczącymi modyfikowanych
wersji dokumentu, z tym wyjątkiem, iż tłumaczenie niniejszego pozwolenia
musi zostać zatwierdzone przez Free Software Foundation lub pozostać w
brzmieniu oryginalnym.
</para>
<para>
Powyższe tłumaczenie <emphasis role="strong">nie</emphasis> zostało
zatwierdzone przez Free Software Fundation i w związku z tym <emphasis
role="strong">nie</emphasis> jest w żadnym stopniu wiążące.  Ma jedynie
charakter informacyjny, a w przypadku redystrybucji lub modyfikacji dokumentu
należy stosować się do oryginału zamieszczonego poniżej.
</para>
<para>
Permission is granted to make and distribute verbatim copies of this document
provided the copyright notice and this permission notice are preserved on all
copies.
</para>
<para>
Permission is granted to copy and distribute modified versions of this document
under the conditions for verbatim copying, provided that the entire resulting
derived work is distributed under the terms of a permission notice identical to
this one.
</para>
<para>
Permission is granted to copy and distribute translations of this document into
another language, under the above conditions for modified versions, except that
this permission notice may be included in translations approved by the Free
Software Foundation instead of in the original English.
</para>
</legalnotice>

</bookinfo>

<!-- XInclude list start -->
<xi:include href="basic-defs.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="getting.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="compat.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="software.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="ftparchives.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="pkg-basics.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="pkgtools.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="uptodate.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="kernel.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="customizing.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="support.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="contributing.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="redistrib.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="nexttime.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<xi:include href="faqinfo.dbk" xmlns:xi="http://www.w3.org/2003/XInclude"/>
<!-- XInclude list end -->



</book>

