#!/usr/bin/perl

use warnings;
use strict;

use IO::File;

my $cfh = IO::File->new( "debian/control", 'r' )
  or die "Unable to open debian/control for reading: $!";

my $lfh = IO::File->new( "debian/languages.txt", 'r' )
  or die "Unable to open debian/languages.txt for reading: $!";

# read in languages
my %languages;
while (<$lfh>) {
    chomp;
    my ( $code, $language ) = split /\t/;
    $languages{$code} = $language;
}

# parse control file
my $new_control_file;
my $discard_stanza = 0;
while (<$cfh>) {
    if (/^Package: debian-faq-.*/) {
        $discard_stanza = 1;
    }
    if (/^\n?$/) {
        if ($discard_stanza) {
            $discard_stanza = 0;
            next;
        }
    }
    next if $discard_stanza;
    $new_control_file .= $_;
}
close($cfh);

for my $code ( sort keys %languages ) {
    my $language = $languages{$code};

    # write out the control file stanza
    $new_control_file .= <<EOF;
Package: debian-faq-$code
Priority: optional
Architecture: all
Multi-Arch: foreign
Suggests: pdf-viewer, www-browser
Depends: \${misc:Depends}
Description: Debian Frequently Asked Questions, in $language
 In this package you will find the Debian GNU/Linux FAQ, which gives
 frequently asked questions (with their answers!) about the Debian distribution
 (Debian GNU/Linux and others) and about the Debian project.
 Some answers assume some knowledge of Unix-like operating systems.
 However, as little prior knowledge as possible is assumed: answers to general
 beginners questions will be kept simple.
 .
 The document is supplied in HTML, PDF and plain text.
 .
 This is the translation in $language of the original English FAQ
 (available in the package debian-faq.)

EOF

    # write out the package.install file
    my $install_fh = IO::File->new( "debian/debian-faq-${code}.install", 'w' )
      or die "Unable to open debian/debian-faq-${code}.install for writing: $!";
    print {$install_fh} <<EOF;
usr/share/doc/debian/FAQ/debian-faq.${code}.*
usr/share/doc/debian/FAQ/${code}
EOF
    close($install_fh);

    # write out the package.install file
    my $docbase_fh = IO::File->new( "debian/debian-faq-${code}.doc-base", 'w' )
      or die
      "Unable to open debian/debian-faq-${code}.doc-base for writing: $!";
    print {$docbase_fh} <<EOF;
Document: debian-faq-${code}
Title: Debian FAQ, in ${language}
Author: Ray Dassen, Javier Fernandez-Sanguino, Susan G. Kleinmann,
 Josip Rodin, Sven Rudolph, Chuck Stickelman, Santiago Vila e.a.
Abstract: This document answers questions frequently asked about Debian
 GNU/Linux.
Section: Debian

Format: HTML
Index: /usr/share/doc/debian/FAQ/${code}/index.${code}.html
Files: /usr/share/doc/debian/FAQ/${code}/*.html

Format: text
Files: /usr/share/doc/debian/FAQ/debian-faq.${code}.txt.gz

Format: PDF
Files: /usr/share/doc/debian/FAQ/debian-faq.${code}.pdf.gz
EOF
    close($docbase_fh);
}

# $new_control_file now terminates with two newline, so remove one
chop($new_control_file);
$cfh = IO::File->new( 'debian/control', 'w' )
  or die "Unable to open debian/control for writing: $!";
print {$cfh} $new_control_file;

close($cfh);
